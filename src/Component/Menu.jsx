import React from "react";
import {Navbar,Container,Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Menu() {
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container style={{width:"60%"}}>
          <Navbar.Brand as={Link} to="/home">PP_LunTimeng_11_HW010</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">

              <Nav.Link as={Link} to="/home" >Home</Nav.Link>
              <Nav.Link href="#pricing">Pricing</Nav.Link>

            </Nav>

          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
