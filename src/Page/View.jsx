import React, { useEffect, useState } from "react";
import { Container, Card, Button } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { getDataById } from "../Service/Service";

export default function View() {
  const [imgId, setImgID] = useState({});

  const aid = useParams();






  useEffect(() => {
    const getById = async () => {
      let objAuthor = await getDataById(aid.id);
      setImgID(objAuthor);
    };
    getById();
  }, []);








  return (
    <Container style={{ width: "60%", margin: "20px auto" }}>
      <br />
      <h1>View Data</h1>
      <Card>
        <Card.Header>{aid.id}</Card.Header>
        <Card.Body>
          <Card.Text>
            <Card.Img variant="bottom" src={imgId.image}/>
          </Card.Text>
          <Card.Title>{imgId.name}</Card.Title>
          <Card.Text>{imgId.email}</Card.Text>
          <Button variant="primary" as={Link} to="/home">
            Back
          </Button>
        </Card.Body>
      </Card>
    </Container>
  );
}
