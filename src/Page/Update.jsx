import React, { useEffect, useState } from "react";
import { Container, Col, Card, Form, Row, Button } from "react-bootstrap";
import {  useHistory, useParams } from "react-router";
import { getDataById, insertImge, updateDataById } from "../Service/Service";



export default function Update() {
  const [objAuthor, setObjAuthor] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imgurl, setImgURL] = useState("");
  const [imgFile, setImgFile] = useState(null);
  const [validemail,setValidEmail] = useState('');
  const [validname,setValidName] = useState('');
  const [btnCkeck,setBtnCheck] = useState(true);



  //we ccan write like this: const author = useParam()
  //then, we can use : author.id for get id from PathVarible

  const {id} = useParams();
  const history = useHistory();





  //luntimeng123@gamil.com  
  let valEmail = /^\S+@\S+\.[a-z]{3}$/g;
  let val = valEmail.test(email)
  //LunTimeng
  let valName = /\S\B[A-Za-z]\D*$/g;
  let valN = valName.test(name);


  useEffect(()=>{
      valN = false
  },[])


  //email valid
  useEffect(()=>{
    if(val == true){
       setValidEmail('')
    }else{
      setValidEmail('Invalid email..!')
    }
  },[email,name])



  //name valid
  useEffect(()=>{
      if(valN){
         setValidName('')
         if(name == ""){
            valN = false
         }
      }else{
        setValidName('Invalid Name')
      }
  },[name,email])



  useEffect(()=>{
    if(val && valN){
       setBtnCheck(false)
    }else{
      setBtnCheck(true)
    }
  },[name,email])







  //componentDidMount

  useEffect(() => {
    const getauthorById = async () => {

      let objAuthor = await getDataById(id);

      setObjAuthor(objAuthor);
      setImgURL(objAuthor.image)

    };

    getauthorById();

  }, []);







//update data to api

  const updateDataToApi = async(e) => {

    e.preventDefault();

    let url = imgFile && await insertImge(imgFile); //uploade image file to api

    let newAuthor = {//new Author which override on old
        name:name?name : objAuthor.name,
        email:email?email : objAuthor.email,
        image:url?url:imgurl
    }

     const mesUpdate = await updateDataById(id,newAuthor); //update data of author
     history.push('/home');//back to home
    alert(mesUpdate)
  }










  return (

    <Container style={{ width: "60%", margin: "20px auto" }}>
      <br />
      <h1>Update</h1>
      <br />
      <Row>

        <Col md={8}>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>AuthorName</Form.Label>
              <Form.Control
                id="user"
                type="text"
                placeholder={objAuthor.name}
                onChange={(e) => setName(e.target.value)}
              />
              <p style={{color:"red"}}>{validname}</p>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Email</Form.Label>
              <Form.Control
                id="email"
                type="email"
                placeholder={objAuthor.email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <p style={{color:"red"}}>{validemail}</p>
            </Form.Group>

            <Button
            disabled={btnCkeck} 
            variant="primary" 
            type="submit" 
            onClick={updateDataToApi}>
              Submit
            </Button>

          </Form>
        </Col>




        {/* image place--------------- */}
        <Col md={4}>
          <Card>
            <label for="myfile">
                Change image
                <Card.Img className="w-100" src={imgurl} />
            </label>
            <Card.Body>
              <input
                type="file"
                id="myfile"
                onChange={(e) => {
                  //get file image from our local computer and convert it to url
                  let url = URL.createObjectURL(e.target.files[0]);
                  //set it state
                  setImgURL(url)
                  //set it to ImgFile
                  setImgFile(e.target.files[0])
                }}
                style={{display:"none"}}
              />
            </Card.Body>
          </Card>
        </Col>

      </Row>
    </Container>
  );
}
