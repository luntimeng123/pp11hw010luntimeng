import React, { useState, useEffect } from "react";
import {
  Container,
  Button,
  Col,
  Row,
  Form,
  Card,
  Table,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  deleteDataFromApi,
  getAllDataFromAuthor,
  insertDataToApi,
  insertImge,
} from "../Service/Service";

export default function Home() {



  const [author, setAuthor] = useState([]);
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [imgurl, setImgURL] = useState(
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzSc0E_-ezcw1juku7x_q9rIVtGDEFGDsZnA&usqp=CAU"
  );
  const [imgFile, setImgFile] = useState(null);
  const [num, setNUM] = useState(0);
  const [validemail,setValidEmail] = useState('');
  const [validname,setValidName] = useState('');
  const [btnCkeck,setBtnCheck] = useState(true);








  //luntimeng123@gamil.com  
  let valEmail = /^\S+@\S+\.[a-z]{3}$/g;
  let val = valEmail.test(email)
  //LunTimeng
  let valName = /\S\B[A-Za-z]\D*$/g;
  let valN = valName.test(name);


  useEffect(()=>{
      valN = false
  },[])



  //email valid
  useEffect(()=>{
    if(val == true){
       setValidEmail('')
    }else{
      setValidEmail('Invalid email..!')
    }
  },[email,name])




  //name valid
  useEffect(()=>{
      if(valN){
         setValidName('')
         if(name == ""){
            valN = false
         }
      }else{
        setValidName('Invalid Name')
      }
  },[name,email])





  useEffect(()=>{
    if(val && valN){
       setBtnCheck(false)
    }else{
      setBtnCheck(true)
    }
  },[name,email])






  useEffect(() => {
    const fetch = async () => {
      //get data from method getAllFromArticle
      let result = await getAllDataFromAuthor();
      setAuthor(result);
    };
    fetch();
  }, [num]);





  //loop table row
  const arrAuthor = author.map((item, index) => (
    <tr key={index}>
      <td>{item._id}</td>
      <td>{item.name}</td>
      <td>{item.email}</td>
      <td>
        <Card.Img variant="top" src={item.image} style={{ width: "100%" }} />
      </td>
      <td>
        <Button
          variant="outline-warning"
          style={{ marginBottom: "5px" }}
          size="sm"
          as={Link}
          to={`/view/${item._id}`}
        >
          View
        </Button>
        <br />
        <Button
          style={{ marginBottom: "5px" }}
          variant="outline-danger"
          size="sm"
          onClick={() => deleteData(item._id)}
        >
          Dele
        </Button>
        <Button variant="outline-success" size="sm" as={Link} to={`/update/${item._id}`}>
          Edit
        </Button>
      </td>
    </tr>
  ));





  //delete Handler
  const deleteData = async (id) => {
    //delete method return data as string  ,in case delete only from api
    let messange = await deleteDataFromApi(id);

    const temp = author.filter(item=>{
       return item._id != id
    })
    setAuthor(temp)
  };






  //insert handler
  const insertData = async (e) => {

    e.preventDefault();


    let url = imgFile &&  await insertImge(imgFile);

    // if (imgFile) {
    //   let url = await insertImge(imgFile);
    //   objAuthor.image = url; //create more property for objAuthor by another way
    // } else {
    //   objAuthor.image =
    //     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzSc0E_-ezcw1juku7x_q9rIVtGDEFGDsZnA&usqp=CAU";
    // }

    let objAuthor = {
      name,
      email,
      image:url?url:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzSc0E_-ezcw1juku7x_q9rIVtGDEFGDsZnA&usqp=CAU"
    };


    let messageI = await insertDataToApi(objAuthor);

    refresh();
    alert(messageI);
    setNUM(num + 1);
  };








  const refresh = (e) => {
    document.getElementById("user").value = "";
    document.getElementById("email").value = "";
  };










  return (
    <Container style={{ width: "60%", margin: "20px auto" }}>
      <h1>Author</h1>
      <br />
      <br />
      <Row>



        <Col md={8}>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>AuthorName</Form.Label>
              <Form.Control
                id="user"
                type="text"
                placeholder="AuthorName"
                onChange={(e) => setName(e.target.value)}
              />
              <p style={{color:"red"}}>{validname}</p>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                id="email"
                type="email"
                placeholder="Email@gamil.com"
                onChange={(e) => setEmail(e.target.value)}
              />
              <p style={{color:"red"}}>{validemail}</p>
            </Form.Group>

            <Button 
            disabled={btnCkeck}
            variant="primary" 
            type="submit" 
            onClick={insertData}
            >
              Submit
            </Button>
          </Form>
        </Col>



        {/* image place--------------- */}
        <Col md={4}>
          <Card>
            <label htmlFor ="myfile">Change image
            <Card.Img src={imgurl} className="w-100" />
            </label>
            <Card.Body>
              <input
                id="myfile"
                type="file"
                onChange={(e) => {
                  //get file image from our local computer and convert it to url
                  let url = URL.createObjectURL(e.target.files[0]);
                  //set it state
                  setImgURL(url);
                  //set it to imgFile
                  setImgFile(e.target.files[0]);
                }}
                style={{display:"none"}}
              />
            </Card.Body>
          </Card>
        </Col>



      </Row>

      {/* table hanfdler */}
      <div>
        <br />
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>{arrAuthor}</tbody>
        </Table>
      </div>
    </Container>
  );
}
