import api from "../API/api";








//service for get author data from api--

export const getAllDataFromAuthor = async () => {
  try{
    let responAuthor = await api.get("author");
    return responAuthor.data.data;
  }catch(error){
      console.log("youshould handle: ", error);
  }
};





//service get data from api by id

export const getDataById = async (id) => {
  try {
    let responObjAuthor = await api.get("author/" + id);
    return responObjAuthor.data.data;
  } catch (error) {
    console.log("fetcherror ", error);
  }
};






//update data to api
export const updateDataById = async (id, auth) => {
  try {

    let responUpdate = await api.put("/author/"+id,auth)

    return responUpdate.data.message

  } catch (error) {
    console.log("you should handle it: ", error);
  }
};







//service for delete data from api by id--

export const deleteDataFromApi = async (id) => {
  try{
    let respone = await api.delete("author/" + id);
    return respone.data.message;
  }catch(error){
      console.log("you should handle: ",error);
  }
};







//service for insert data to api

export const insertDataToApi = async (auth) => {
  try{
    let respon = await api.post("author", auth);
    return respon.data.message;
  }catch(error){
      console.log("you should handle: ",error);
  }
};








//uploade image to api
export const insertImge = async (imageFile) => {
  try{
      //flow upload image to api

  let formData = new FormData();
  formData.append("image", imageFile);

  let respone = await api.post("images", formData);

  return respone.data.url;
  }catch(error){
      console.log('you should handle: ',error);
  }
};
