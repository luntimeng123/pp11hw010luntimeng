import React from "react";
import Menu from "./Component/Menu";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Home from "./Page/Home";
import View from "./Page/View";
import Update from "./Page/Update";

export default function App() {
  return (
    <Router>
      <Menu />
      <Switch>
        <Route path="/home" render={() => <Home />} />
        <Route path="/view/:id" render={() => <View/>} />
        <Route path="/update/:id" render={()=><Update/>} />
      </Switch>
    </Router>
  );
}
