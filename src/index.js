import React from 'react'
import ReactROM, { render } from 'react-dom'
import App from './App'


ReactROM.render(
    <App/>,
    document.getElementById('root')
)